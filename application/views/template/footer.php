    <!-- FOOTER --> 
    <div class="container">
          <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
              <small class="d-block mb-3 text-muted">&copy; 2017-2018 - <a href="http://www.soyabeansph.com/">www.soyabeansph.com</a></small>
          </div>
          <div class="col-6 col-md">
            <h5>Features</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="http://www.soyabeansph.com/">Home</a></li>
              <li><a class="text-muted" href="http://www.soyabeansph.com/schedule">Schedule</a></li>
              <li><a class="text-muted" href="http://www.soyabeansph.com/contact">Contact</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Call Us!</h5>
            <ul class="list-unstyled text-small">    
             <li><span class="fa fa-phone"></span>&nbsp;(032) 416 0622</li>
             <li><span class="fa fa-envelope"></span>&nbsp;info@soyabeansph.com</li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Social</h5>
            <a href="https://www.facebook.com/SoyaBeansPH/"><span class="fa fa-facebook-square"></span></a>&nbsp;<a href="#"><span class="fa fa-twitter-square"></span></a>&nbsp;<a href="#"><span class="fa fa-google-plus-square">&nbsp;</span></a>
          </div>
        </div>
      </footer>
      </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
	<script src="<?php echo base_url();?>assets/js/vendor/popper.min.js"></script>  
    <script src="<?php echo base_url();?>assets/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/vendor/holder.min.js"></script>
  </body>
</html>