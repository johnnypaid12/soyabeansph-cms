<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title><?php echo $title; ?></title>
	<meta name="description"  content="<?php echo $desc; ?>" />   
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">  
    <!-- Bootstrap core CSS -->
    <link type="text/css" href="<?php echo base_url(); ?>assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
    <!-- Custom styles for this template -->
    <link type="text/css" href="<?php echo base_url(); ?>assets/css/carousel.css" rel="stylesheet">
    <link type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">  
    <link type="text/css" href="<?php echo base_url(); ?>assets/css/blog.css" rel="stylesheet">  
	  
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119607822-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-119607822-2');
    </script>
  
  </head>
  <body>

<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top nav-color other-font">
    <a class="navbar-brand" href="http://www.soyabeansph.com/">SOYABEANS</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url();?>">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url();?>schedule/">Schedule</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="<?php echo base_url();?>contact/">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link " href="<?php echo base_url();?>blog/">Blog</a>
        </li>
   
      </ul>
    </div>  
  </nav>
</header>


