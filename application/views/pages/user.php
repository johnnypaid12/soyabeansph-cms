
       
        
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4 ">
        <h2>Register</h2>
        <hr>
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
        <?php if($this->session->flashdata('msg')): ?>
          <strong><?php echo $this->session->flashdata('msg'); ?></strong>
        <?php endif; ?>
        </div>
            <?php echo validation_errors(); ?>
            <?php echo form_open('user/register'); ?>
                <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control input-width" name="fname" placeholder="" value="<?php echo set_value('fname'); ?>" required>
                </div>  
                <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control input-width" name="lname" placeholder="" value="<?php echo set_value('lname'); ?>" required>
                </div> 
                <div class="form-group">
                <label>User</label>
                <input type="text" class="form-control input-width" name="username" placeholder="" value="<?php echo set_value('username');?>" required>
                </div> 
                <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control input-width" name="password" placeholder="" value="" required>
                </div>
                <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control input-width" name="passconf" placeholder="" value="" required>
                </div>

                <button type="submit" class="btn btn-primary" name="save">Submit</button>
            <?php echo form_close(); ?>
        </div>
        </main>
      </div> 
    </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

</body>
</html>