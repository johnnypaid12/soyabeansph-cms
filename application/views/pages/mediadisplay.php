    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4 ">
        <h2>Gallery</h2>
        <hr>
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
            <div class="lh-100"> </div>
        </div>  

        <div class="my-3 p-3 bg-white rounded box-shadow">
            <?php foreach($map as $image): ?>
                <a href="<?php echo base_url() . 'assets/img/'. $image; ?>"><img src="<?php echo base_url() . 'assets/img/'. $image; ?>" class="img-thumbnail img-size" title="<?php echo $image; ?>"/></a>
            <?php endforeach; ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="media/imageUploadPost" class="dropzone" >
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
            </div>
        </div>
    </main>



 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="<?php echo base_url();?>assets/dist/js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    </html>
    </body>
