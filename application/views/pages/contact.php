
<main role="main">

<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>    
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="first-slide" src="<?php echo base_url(); ?>assets/img/travel-japan2.jpg" alt="First slide">
      <div class="container other-font">
        <div class="carousel-caption text-left ">
            <h1>Travel</h1>
          <p>Learning Japanese language makes travel more enjoyable as you can converse while you wander around Japan.</p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img class="second-slide" src="<?php echo base_url(); ?>assets/img/career-image.jpg" alt="Second slide">
      <div class="container other-font">
        <div class="carousel-caption">
            <h1>Career</h1>
          <p>Broaden your profession choices! Many Japan companies have branches overseas. </p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img class="third-slide" src="<?php echo base_url(); ?>assets/img/culture3.jpg" alt="Third slide">
      <div class="container other-font">
        <div class="carousel-caption text-right">
            <h1>Culture</h1>
          <p>Part of learning a second language is knowing the values and tradition, Japanese is one of most exceptional culture in the world.</p>
        </div>
      </div>
    </div>
  </div> 
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container">
  <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Contact Us</h1>
  </div>


<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid container-fluid-color ">

 <div class="col-sm-6">
  <script src="../assets/js/greetings.js"></script>    
    <p><span class="fa fa-map-marker"></span>&nbsp;&nbsp;&nbsp;M-III Doña Luisa Bldg. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fuente Osmeña Circle<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cebu City 6000, <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Philippines</p>
    <p><span class="fa fa-phone"></span>&nbsp;&nbsp;(032) 416 0622</p>
    <p><span class="fa fa-envelope"></span>&nbsp;&nbsp;info@soyabeansph.com<br><br></p>
  </div>


  <div class="col-sm-6">
   <form method="post" action="email.php">
    <div class="row">
      <div class="col-sm-6 form-group">
        <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
      </div>
      <div class="col-sm-6 form-group">
        <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
      </div>
    </div>
    <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
    <div class="row">
      <div class="col-sm-12 form-group">
        <button class="btn btn-primary btn-lg costum-button costum-button-caro" type="submit">Send</button>
      </div>
    </div>
  </form>
   </div><br>

  <div class="col-sm-12 embed-responsive embed-responsive-16by9" id="googleMap">
    <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3925.3920789500385!2d123.89148085044468!3d10.310473470404057!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a9994ee4ef1e1d%3A0x99d9c443a5dbfb0a!2sDona+Luisa+Bldg.!5e0!3m2!1sen!2sph!4v1527132648685" width="100%" height="400"  allowfullscreen></iframe>
  </div>

</div>
