<div class="container">
                 <hr id="color">  
                 <p><?php echo $length?> conversations:</p>
                    <?php for ($i=0; $i<$length; $i++){  
                        echo '<div class="media border p-3">';
                        echo '<img src="' . base_url() . 'assets/img/avatar.png" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:30px;">';
                        echo '<div class="media-body">';
                        echo '<h4 class="blog-font">' . $full_name[$i] .' <small><i>Posted on ' . $date_submitted[$i] . '</i></small></h4>';
                        echo '<p>' . $message[$i] . '</p>';
                        echo '<small><p style="color:#99b19a;">reply</p></small>';
                        echo '</div>'; 
                        echo '</div>'; } ?>
                  

                  <?php echo form_open('comment/index');?>
                  <?php if($this->session->flashdata('msg')): ?>
                    <?php echo $this->session->flashdata('msg'); ?>
                  <?php endif; ?> 

                  <p><a id="myDIV" href="" data-toggle="collapse" data-target="#addComment" onclick="myFunction()">Add Comment</a></p>
                  <div id="addComment" class="collapse">
                    <div  class="form-group">
                      <?php $data = array (
                        'post_title'=> $blogsection,
                        'slug'=> $slug
                        );?>
                      <?php echo form_hidden($data); ?>
                        <div class="row">
                            <div class="col">
                              <input id="test" type="text" class="form-control" name="name" placeholder="Name" required>
                            </div>
                            <div class="col">
                              <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                            </div>
                        </div>  
                    </div>
                        <textarea class="form-control" rows="5" id="comment" name="text" placeholder="Comment" required></textarea>
                        <button type="submit" class="btn btn-primaryc blog-button" name="submit">Submit</button>
                      <?php form_close();?>
                    </div>
              </div>
          </div>
        </div><!-- /.container -->
