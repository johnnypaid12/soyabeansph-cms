

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4 ">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Comments</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Print</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
          </div>

         <!-- canvas area-->
         
         
          <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
            <div class="lh-100">
              <?php if($this->session->flashdata('msg')): ?>
              <strong style="color:#ffffff;"><?php echo $this->session->flashdata('msg'); ?></strong>
              <?php endif; ?> 
            </div>
          </div>  

   <h6 class="border-bottom border-gray pb-2 mb-0"></h6>

    <?php if (isset($results)) { ?>
            <?php foreach ($results as $data) {  
                echo '<div class="media text-muted pt-3">';
                echo '<img data-src="holder.js/20x60 ?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded admin-icon">';
                echo '<div class="media-body pb-3 mb-0 small border-bottom border-gray">';
                echo '<div class="d-flex justify-content-between align-items-center w-100">';
                echo '<span class="d-block"><strong><a href="' . base_url() . 'blog/links/' . $data->slug . '">' . $data->article_title . '</a></strong><br>' . '<strong>By:</strong> ' . $data->full_name . '<br>' . $data->message . '<br>' 
                                                      . "<strong>Approved:</strong> " . $data->approve_status  . '</span>';
                if($data->approve_status == 'no') { 
                  echo '<a href="'. base_url() . 'comment/updateComment/' . $data->comment_id  .'">Approve comment</a>';
                } else {
                  echo '<a href="'. base_url() . 'comment/updateComment/' . $data->comment_id  .'">Disapprove comment</a>';
                } 
                echo '</div>';
                echo '<strong class="text-gray-dark"><span class="d-block">' . '</span></strong> ';
                // echo '<div class="test2 lh-125"><a href="' . base_url(). 'deleted/recoverBlog/' . '"><span data-feather="save"></span></a></div>';
                echo '</div>'; 
                echo '</div>'; } ?> 
			  
			      <?php } else { ?>
                <div>No comment(s) found.</div>
            <?php } ?>
            
            <small class="d-block text-right mt-3">
                <?php if (isset($links)) { ?>
                    <?php echo $links ?>
                <?php } ?>
            </small>
    </div>
</main>
</div> 
</div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="<?php echo base_url();?>assets/dist/js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    </html>
    </body>
