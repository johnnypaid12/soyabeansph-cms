<main role="main">

      <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>    
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="<?php echo base_url(); ?>assets/img/travel-japan2.jpg" alt="First slide">
            <div class="container other-font">
              <div class="carousel-caption text-left ">
                  <h1 class="text-left">Travel</h1>
                <p class="lead">Learning Japanese language makes travel more enjoyable as you can converse while you wander around Japan.</p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="<?php echo base_url(); ?>assets/img/career-image.jpg" alt="Second slide">
            <div class="container other-font">
              <div class="carousel-caption">
                  <h1 class="text-left">Profession</h1>
                <p class="lead">Broaden your career choices! Many Japan companies have branches overseas. </p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="third-slide" src="<?php echo base_url(); ?>assets/img/culture3.jpg" alt="Third slide">
            <div class="container other-font">
              <div class="carousel-caption">
                  <h1 class="text-left">Culture</h1>
                <p class="lead">Part of learning a second language is knowing the values and tradition, Japanese is one of most exceptional culture in the world.</p>
              </div>
            </div>
          </div>
        </div> 
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>


      <!-- Marketing messaging and featurettes
      ================================================== -->
      <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container">
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
          <h1 class="display-4">Schedule and Pricing</h1>
          <p class="lead">Checkout our Japanese language courses, quotes and schedules.</p>
          <p class="lead">With our experienced native Japanese instructor she will support your learning to widen your career opportunity and unlock one of the world’s largest economy and its distinctive culture.</p>
         <p class="lead"><span class="badge badge-secondary">Free Demo</span>&nbsp;We also cater 1 to 1 classes or by group.</p>
        </div>
    </div>

    <div class="container">
        
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">KICCHIRI</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
                <li><h2>JLPT N5</h2></li>
                <li>Grammar</li>
              <li>Conversation</li>
              <li>Listening</li>
                <li>HIRAGANA</li>
                <li>KATAKANA</li> 
                <li>KANJI</li>
				<li><button id="myBtn" class="btn btn-info"><h3>Click for Price</h3></button>
				`<div id="myModal" class="modal">
					<!-- Modal content -->
					<div class="modal-content">
						<span class="close">&times;</span>
						<h1 id="price">₱ 7,000 / month</h1>
						<p class="price">COURSE:<br><strong>KICCHIRI</strong></p>
						<img src="<?php echo base_url(); ?>assets/img/japanese-flag.png" alt="Japanese flag icon.">
						
						
					</div>
				</div>

				</li>
              <li><hr></li>
              <li>2h - Wednesday &amp; Friday</li>  
              <li><hr></li>
              <li>2 times a week / 4 weeks</li>
            </ul>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">MOTTO KICCHIRI</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
                <li><h2>JLPT N4</h2></li>
              <li>Grammar</li>
              <li>Conversation</li>
              <li>Listening</li>
              <li>HIRAGANA</li>
              <li>KATAKANA</li> 
              <li>KANJI</li>
			  <li><button type="button" id="myBtn2" class="btn btn-info"><h3>Click for Price</h3></button>
				`<div id="myModal" class="modal">
					<!-- Modal content -->
					<div class="modal-content">
						<span class="close">&times;</span>
						<h1>₱ 7,000 / month</h1>
						<p class="price">COURSE:<br><strong>MOTTO KICCHIRI</strong></p>
						<img src="<?php echo base_url(); ?>assets/img/japanese-flag.png" alt="Japanese flag image.">
					</div>
				</div>
			  </li>
              <li><hr></li>
              <li>2h - Tuesday &amp; Thursday</li>  
              <li><hr></li>
              <li>2 times a week / 4 weeks</li>
            </ul>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">KAIGOSHI-SAN (CAREGIVER)</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
                <li><h2>JLPT N5+N4</h2></li>
              <li>Technical Terms</li>
              <li>Japanese Culture<br><br><br><br><br></li>
			  <li><button type="button" id="myBtn2" class="btn btn-info"><h3>Click for Price</h3></button>
				`<div id="myModal" class="modal">
					<!-- Modal content -->
					<div class="modal-content">
						<span class="close">&times;</span>
						<h1>₱ 8,000 / month</h1>
						<p class="price">COURSE:<br><strong>KAIGOSHI-SAN (CAREGIVER)</strong></p>
						<img src="<?php echo base_url(); ?>assets/img/japanese-flag.png" alt="Japanese flag picture.">		
					</div>
				</div>
			  </li>	
              <li><hr></li>
              <li>2h - Monday - Friday</li>  
              <li><hr></li>
              <li>5 times a week / 4 weeks</li>
            </ul>
          
          </div>
        </div>
        </div>
        
        <div class="card-deck mb-3 text-center">
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">OSYABERI</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
              <li>Conversation</li>
              <li><br><br><br><br><br><br></li> 
			  <li><button type="button" id="myBtn2" class="btn btn-info"><h3>Click for Price</h3></button>
				`<div id="myModal" class="modal">
					<!-- Modal content -->
					<div class="modal-content">
						<span class="close">&times;</span>
						<h1>₱ 6,000 / month</h1>
						<p class="price">COURSE:<br><strong>OSYABERI</strong></p>
						<img src="<?php echo base_url(); ?>assets/img/japanese-flag.png" alt="Flag of Japan.">		
					</div>
				</div>
			  </li>		
              <li><hr></li>
              <li>1.5 - Thursday &amp; Saturday</li>  
              <li><hr></li>
              <li>2 times a week / 4 weeks</li>
            </ul>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">AIUEO</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
              <li>HIRAGANA</li>
              <li>KATAKANA</li>
              <li><br><br><br><br><br></li>
			  <li><button type="button" id="myBtn2" class="btn btn-info"><h3>Click for Price</h3></button>
				`<div id="myModal" class="modal">
					<!-- Modal content -->
					<div class="modal-content">
						<span class="close">&times;</span>
						<h1>₱ 6,000 / month</h1>
						<p class="price">COURSE:<br><strong>AIUEO</strong></p>
						<img src="<?php echo base_url(); ?>assets/img/japanese-flag.png" alt="Japanese flag.">
					</div>
				</div>
			  </li>	
              <li><hr></li>
              <li>1.5h - Tuesday &amp; Thursday</li>  
              <li><hr></li>
              <li>2 times a week / 4 weeks</li>
            </ul>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">KANJI</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
              <li><h2>JLPT N5 Level</h2></li>
              <li>KANJI</li>
              <li><br><br><br><br></li>
			  <li><button type="button" id="myBtn2" class="btn btn-info"><h3>Click for Price</h3></button>
				`<div id="myModal" class="modal">
					<!-- Modal content -->
					<div class="modal-content">
						<span class="close">&times;</span>
						<h1>₱ 6,000 / month</h1>
						<p class="price">COURSE:<br><strong>KANJI</strong></p>
						<img class="flag" src="<?php echo base_url(); ?>assets/img/japanese-flag.png" alt="Japan icon image.">
					</div>
				</div>
			  </li>	
              <li><hr></li>
              <li>1.5h - Wednesday &amp; Friday</li>  
              <li><hr></li>
              <li>2 times a week / 4 weeks</li>
            </ul>
          
          </div>
        </div>
        </div>
        </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
	<script>
	// Get the modal
	var modals = document.getElementsByClassName('modal');

	// Get the button that opens the modal
	var btns = document.getElementsByClassName("btn");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close");
		
	for(let i=0;i<btns.length;i++){
    btns[i].onclick = function() {
      modals[i].style.display = "block";
    	}
	}

	for(let i=0;i<span.length;i++){
    span[i].onclick = function() {
        modals[i].style.display = "none";
    	}
	}
	
	</script>
