<main role="main">

<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>    
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="first-slide" src="<?php echo base_url(); ?>assets/img/travel-japan2.jpg" alt="First slide">
      <div class="container other-font">
        <div class="carousel-caption">
            <h1 class="caro-h1 text-left">Travel</h1>
          <p class="lead">Learning Japanese language makes travel more enjoyable as you can converse while you wander around Japan.</p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img class="second-slide" src="<?php echo base_url(); ?>assets/img/career-image.jpg" alt="Second slide">
      <div class="container other-font">
        <div class="carousel-caption">
            <h1 class="caro-h1 text-left">Profession</h1>
          <p class="lead">Broaden your career choices! Many Japan companies have branches overseas. </p>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img class="third-slide" src="<?php echo base_url(); ?>assets/img/culture3.jpg" alt="Third slide">
      <div class="container other-font">
        <div class="carousel-caption text-left">
            <h1 class="caro-h1 text-left">Culture</h1>
          <p class="lead">Part of learning a second language is knowing the values and tradition, Japanese is one of most exceptional culture in the world.</p>
        </div>
      </div>
    </div>
  </div> 
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container">
  <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Oopss! Sorry page not found..<br><br></h1>
    <h1 class="text-center"><a href="http://www.soyabeansph.com/"><span class="badge badge-info">Click Here</span></a></h1>
      
  </div>
