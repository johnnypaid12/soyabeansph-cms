
<?php $this->load->helper('form');?>
<body class="text-center bg-img">
    <form class="form-signin" method ="post" action="<?php echo base_url();?>admin/login_validation" >
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <?php   echo '<label class="text-danger">'.$this->session->flashdata ("error").'</label>';  ?> 
      <label for="inputEmail" class="sr-only">Username</label>
      <input type="text" id="inputText" class="form-control" name="username" required>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control" name="password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block costum-button" type="submit" name="insert" value="Login">Sign in</button>
       
    </form>
  </body>