
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4 ">
        <h2>Add Post</h2>
        <hr>
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
          <?php if($this->session->flashdata('msg')): ?>
            <strong style="color:#ffffff;"><?php echo $this->session->flashdata('msg'); ?></strong>
          <?php endif; ?>
        </div>
        
          <form id="my-textbox" action="post/initializePost" enctype="multipart/form-data" method="post" accept-charset="utf-8" class="formbox">
            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control" name = "title" placeholder="Add Title" value="" required>
            </div>
            <div class="form-group">
              <label>Body</label>
              <textarea name="editor1" placeholder="Add Body">Edit this content...</textarea>
            </div>
            <div class="form-group">
              <label>Meta Title</label>
              <input type="text" class="form-control" name="metatitle" placeholder="Add Meta Title" value="">
            </div>
            <div class="form-group">
              <label>Meta Description</label>
              <input type="text" class="form-control" name="description" placeholder="Add Description" value="">
            </div>
            <div class="form-group">
              <label>Status</label>
                <select name="category_id" class="form-control input-width">
                  <option value="published">Publish</option>
                  <option value="draft">Draft</option>
                </select>
            </div>
            <div class="form-group">
              <label>Image Alt Text</label>
              <input type="text" class="form-control input-width" name="altext" placeholder="Add Description" value="">
            </div>
            <div class="form-group">
              <label>Upload Image</label><br>
              <input type="file" name="userfile" size="20">
            </div>  
              <button type="submit" class="btn btn-primary" name="save">Submit</button>
          </form>
          
      </div>
      <script>
          CKEDITOR.replace( 'editor1' );
      </script>
         
        </main>
      </div> 
    </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

</body>
</html>