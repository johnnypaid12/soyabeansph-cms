<main role="main">

        <div class="container marketing">
          <div class="jumbotron">
              <div class="container">
                <h1 class="display-3"><?php echo $blogsection; ?></h1>
                <p>By: <?php echo ucfirst($first_name) . ' ' . ucfirst($last_name); ?><br></p>
                <p><?php //echo $date_posted; ?></p>
                <img class="text-center" src="<?php echo base_url(); ?>assets/img/travel-japan2.jpg" alt="" style="width:100%; height:20rem; margin-left:auto; margin-right: auto; display:block;">
                <p><?php echo $body; ?></p>
              </div>

              <div class="container">
                 <hr id="color">  
                 <p><?php echo $length ?> conversations:</p>
                  <?php if ($length > 0) { ;?>

                    <?php for ($i=0; $i<$length; $i++){  
                        echo '<div class="media border p-3">';
                        echo '<img src="' . base_url() . 'assets/img/avatar.png" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:30px;">';
                        echo '<div class="media-body">';
                        echo '<h4 class="blog-font">' . $full_name[$i] .' <small><i>Posted on ' . $date_submitted[$i] . '</i></small></h4>';
                        echo '<p>' . $message[$i] . '</p>';
                        //echo '<small><p style="color:#99b19a;"><a href="">reply</a></p></small>';
                        echo '</div>'; 
                        echo '</div>'; } ?>
                  <?php }?>

                  <?php echo form_open('comment/index');?>
                  <?php if($this->session->flashdata('msg')): ?>
                    <?php echo $this->session->flashdata('msg'); ?>
                  <?php endif; ?> 

                  <p><a id="myDIV" href="" data-toggle="collapse" data-target="#addComment" onclick="myFunction()">Add Comment</a></p>
                  <div id="addComment" class="collapse">
                    <div  class="form-group">
                      <?php $data = array (
                        'post_title'=> $blogsection,
                        'slug'=> $slug
                        );?>
                      <?php echo form_hidden($data); ?>
                        <div class="row">
                            <div class="col">
                              <input id="test" type="text" class="form-control" name="name" placeholder="Name" required>
                            </div>
                            <div class="col">
                              <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                            </div>
                        </div>  
                    </div>
                        <textarea class="form-control" rows="5" id="comment" name="text" placeholder="Comment" required></textarea>
                        <button type="submit" class="btn btn-primaryc blog-button" name="submit">Submit</button>
                      <?php form_close();?>
                    </div>
              </div>

          </div>
        </div><!-- /.container -->

        <script>

          function myFunction() {
              var x = document.getElementById("myDIV");
              if (x.innerHTML === "Add Comment") {
                  x.innerHTML = "Cancel Comment"; 
              } else {
                  x.innerHTML = "Add Comment";
              }
          }

      </script>
  