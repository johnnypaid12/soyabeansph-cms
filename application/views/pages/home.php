<main role="main">
		
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>    
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="first-slide" src="<?php echo base_url(); ?>assets/img/travel-japan2.jpg" alt="First slide">
              <div class="container other-font">
                <div class="carousel-caption">
                    <h1 class="caro-h1 text-left">Travel</h1>
                  <p class="lead">Learning Japanese language makes travel more enjoyable as you can converse while you wander around Japan.</p>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="second-slide" src="<?php echo base_url(); ?>assets/img/career-image.jpg" alt="Second slide">
              <div class="container other-font">
                <div class="carousel-caption">
                    <h1 class="caro-h1 text-left">Profession</h1>
                  <p class="lead">Broaden your career choices! Many Japan companies have branches overseas. </p>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <img class="third-slide" src="<?php echo base_url(); ?>assets/img/culture3.jpg" alt="Third slide">
              <div class="container other-font">
                <div class="carousel-caption text-left">
                    <h1 class="caro-h1 text-left">Culture</h1>
                  <p class="lead">Part of learning a second language is knowing the values and tradition, Japanese is one of most exceptional culture in the world.</p>
                </div>
              </div>
            </div>
          </div> 
          <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
  
  
        <!-- Marketing messaging and featurettes
        ================================================== -->
        <!-- Wrap the rest of the page in another container to center all the content. -->
  
        <div class="container marketing">
  
        <div class="jumbotron">
          <div class="container">
            <h1 class="display-3">SOYA BEANS</h1>
            <p>- Japanese Language School<br></p>	
            <p id="font-italic">"There are no national frontiers to learning." - Japanese Proverbs</p>
            <p class="lead">A full-fledged Japanese language school has opened here in Cebu! SoyaBeans is very willing to help you especially with your dream in traveling, studying or working in Japan. We have Japanese sensei that could help you learn Japanese language in an easy and enjoyable manner.</p> 
            <p class="lead">We offer N5, N4, N3, for tourist or business short term courses and even writing programs. In less than no time, you will already be able to write and speak Nihongo well and fly in your dream destination. And of course not only Japanese is learned! The culture, mentality and their etiquette is also included in the content of the study.</p>
            <p class="lead">For those who want to be a caregiver or like to work at a nursing home, we have a special course for you.</p>    
            <p class="lead">Enroll now and learn with us because you don’t have to figure it out yourself. We are always here to help!</p>    
            <p ><a class="btn btn-primary btn-lg costum-button costum-button-caro" href="http://www.soyabeansph.com/schedule" role="button">Learn more &raquo;</a></p>
          </div>
        </div>
      </div><!-- /.container -->
  