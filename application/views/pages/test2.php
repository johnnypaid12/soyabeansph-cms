

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4 ">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary" onclick="window.print()">Print</button>
                <a href="<?php echo base_url() . 'pdf/save_download'?>"><button class="btn btn-sm btn-outline-secondary">Export</button></a>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
          </div>

         <!-- canvas area-->
         
         
          <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
            <div class="lh-100">
              <?php if($this->session->flashdata('msg')): ?>
              <strong style="color:#ffffff;"><?php echo $this->session->flashdata('msg'); ?></strong>
              <?php endif; ?> 
            </div>
          </div>  
          <div class="my-3 p-3 bg-white rounded box-shadow">
          <h6 class="border-bottom border-gray pb-2 mb-0">Published Articles</h6>

          <?php $color = array ('e83e8c','007bff','6f42c1','ffc107','28a745');?>  
          <?php if (isset($results)) { $i=0;?>
            <?php foreach ($results as $data) {  
                
                echo '<div class="media text-muted pt-3">';
                echo '<img data-src="holder.js/20x50 ?theme=thumb&bg=' . $color[$i] . '&fg=' . $color[$i] .'&size=1" alt="" class="mr-2 rounded admin-icon">';
                echo '<div class="media-body pb-3 mb-0 small border-bottom border-gray">';
                echo '<div class="d-flex justify-content-between align-items-center w-100">';
                echo '<span class="d-block">@' . $data->first_name . ' ' . $data->last_name . '</span>';
                echo '</div>';
                echo '<strong class="text-gray-dark"><span class="d-block">' . $data->post_title . '</span></strong>';
                echo '<div class="test2 lh-125"><a href="' . base_url(). 'blog/links/' . $data->slug .'"><span data-feather="monitor" ></span></a> | <a href="' . base_url(). 'post/deleteBlog/' . $data->post_id .'"><span data-feather="trash-2" ></span></a> | <a href="' . base_url(). 'post/editBlog/' . $data->post_id .'"><span data-feather="edit"></span></a></div>';
                echo '</div>'; 
                echo '</div>'; $i++;} ?> 
			  
			      <?php } else { ?>
                <div>No user(s) found.</div>
            <?php } ?>
            
            <small class="d-block text-right mt-3">
                <?php if (isset($links)) { ?>
                    <?php echo $links ?>
                <?php } ?>
            </small>
          </div>


          <div class="my-3 p-3 bg-white rounded box-shadow">
          <h6 class="border-bottom border-gray pb-2 mb-0">Soya Beans News</h6>
          <div class="media text-muted pt-3">
            <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
              <div class="d-flex justify-content-between align-items-center w-100">
                <strong class="text-gray-dark">Full Name</strong>
                <a href="#">Follow</a>
              </div>
              <span class="d-block">@username</span>
            </div>
          </div>
          <div class="media text-muted pt-3">
            <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
              <div class="d-flex justify-content-between align-items-center w-100">
                <strong class="text-gray-dark">Full Name</strong>
                <a href="#">Follow</a>
              </div>
              <span class="d-block">@username</span>
            </div>
          </div>
          <div class="media text-muted pt-3">
            <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
              <div class="d-flex w-100">
                <strong class="text-gray-dark">Full Name</strong>
              </div><div class="test2"><a href="#">Follow</a></div>
              <span class="d-block">@username</span>
            </div>
          </div>
          <small class="d-block text-right mt-3">
            <a href="#">All suggestions</a>
          </small>
        </div>

     
         
        </main>
      </div> 
    </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="<?php echo base_url();?>assets/dist/js/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
    </html>
    </body>
