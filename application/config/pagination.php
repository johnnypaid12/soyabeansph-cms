<?php
        
      // custom paging configuration
      $config['num_links'] = 2;
      $config['use_page_numbers'] = TRUE;
      $config['reuse_query_string'] = TRUE;
       
      $config['full_tag_open'] = '<div class="pagination">';
      $config['full_tag_close'] = '</div>';
       
      $config['first_link'] = 'First Page';
      $config['first_tag_open'] = '<div class="page-link">';
      $config['first_tag_close'] = '</div>';
       
      $config['last_link'] = 'Last Page';
      $config['last_tag_open'] = '<div class="page-link">';
      $config['last_tag_close'] = '</div>';
       
      $config['next_link'] = 'Next Page';
      $config['next_tag_open'] = '<div class="page-link">';
      $config['next_tag_close'] = '</div>';

      $config['prev_link'] = 'Prev Page';
      $config['prev_tag_open'] = '<div class="page-link">';
      $config['prev_tag_close'] = '</div>';

      $config['cur_tag_open'] = '<div class="page-link">';
      $config['cur_tag_close'] = '</div>';

      $config['num_tag_open'] = '<div class="page-link">';
      $config['num_tag_close'] = '</div>';