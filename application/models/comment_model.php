<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		
    }


    // display comment in admin section 
    public function getComment($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->select('*');
        $this->db->from('tblcomment');
        //$this->db->where('approve_status', 'no');
        $query = $this->db->get();

        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }    
            return $data;
        }
        return false;
    }

    function saveComment($data) {
       $query = $this->db->insert('tblcomment', $data);

       if (!$query) {
           return false;
       } else {   
           return true;
       }
    }

    function get_total_comment($approve_status) {      
        $this->db->select('*');
        $this->db->from('tblcomment');
        $this->db->where('approve_status', $approve_status);
        $query = $this->db->get();

        return $query->num_rows();
    }

    function changeCommentStatus($id) {
        $getStat = $this->db->query("SELECT approve_status FROM tblcomment WHERE comment_id = $id");
        foreach ($getStat->result() as $row) {
            $getStat = $row->approve_status;
        }
        switch ($getStat) {
            case 'no':
                $changeStat = 'yes';
                break;
            case 'yes':
            $changeStat = 'no';
                break;    
        }
        $this->db->set('approve_status', $changeStat);
        $this->db->where('comment_id', $id);
        $this->db->update('tblcomment');

        return $this->db->affected_rows();
    }

}