<?php

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Pagination_model extends CI_Model {

    function __construct() {
     
        parent::__construct();
        $this->load->database();
    }
 
    
    function displayArticles($limit, $start, $del_status)  {

        $this->db->limit($limit, $start);
        $this->db->select('tbltest.author, tbltest.post_title, tbltest.post_id, tbltest.slug, tbltest.date_posted, tbluser.first_name, tbluser.last_name');
        $this->db->from('tbltest');
        $this->db->join('tbluser', 'tbltest.author=tbluser.username');
        $this->db->where('tbltest.deleted', $del_status);
        $this->db->order_by("date_posted", "desc"); 
        $query = $this->db->get();
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }    
            return $data;
        }
 
        return false;

    }

    public function get_total_rec($del_status) 
    {
        $this->db->select('*');
        $this->db->from('tbltest');
        $this->db->where('deleted', $del_status);
        $query = $this->db->get();

        return $query->num_rows();
    }

    
}