<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{

	public function __construct() {
		parent::__construct();
		$this->load->database();
		
    }
    

    function checkUserName($username) {

        $this->db->where('username',$username);
        $query = $this->db->get('tbluser');
       // $query = $this->db->query("SELECT user_name, body, metatitle, meta_description, post_status, altext, post_image FROM tbltest  WHERE post_id = $post_id");

        if (empty($query->row_array())){
            return true;
        } else {
            return false;
        }
    }

    function registerUser($data) {

        $this->db->insert('tbluser',$data);
    
    }


}