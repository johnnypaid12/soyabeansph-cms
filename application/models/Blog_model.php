<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();	
	}

	function saveData ($data) {
		$test = $this->db->insert('tbltest', $this->db->escape_str($data));
		if (!$test) {
			$error = $this->db->error();
			if ($error['code'] == 1062) {
				return false;
			} else {
				$this->db->insert('tbltest', $this->db->escape_str($data));
			} 
		} 
			return true;	
	}

	function updatePost($data) {
		$this->db->set('post_title', $data['post_title']);
		$this->db->set('body', $data['body']);
		$this->db->set('post_status', $data['post_status']);
		$this->db->set('post_image', $data['post_image']);
		$this->db->set('metatitle', $data['metatitle']);
		$this->db->set('meta_description', $data['post_title']);
		$this->db->set('altext', $data['post_title']);
		$this->db->where('post_title', $data['post_title']);

		$this->db->update('tbltest', $this->db->escape_str($data));		
	}

	// delete or recover article
	function RecDelPost($post_id, $tag) {

		$this->db->query("UPDATE tbltest SET deleted = '$tag' WHERE post_id = $post_id");
	}
 
	function searchPost($post_id)  {
		$this->db->select('post_title, body, metatitle, meta_description, post_status, altext, post_image');
        $this->db->from('tbltest');
		$this->db->where('post_id', $post_id);
		$query = $this->db->get();
		return $query;
	}

	//blog search and its page comment display 
	function displayPost($slug) {

		$this->db->select('tbltest.post_title, tbltest.body, tbltest.date_posted, tbltest.post_id, tbltest.post_image, tbltest.slug, tbltest.metatitle, tbltest.meta_description, tbltest.altext, tbltest.date_posted, tbluser.first_name, tbluser.last_name,tblcomment.full_name, tblcomment.message, tblcomment.date_submitted');
        $this->db->from('tbltest');
        $this->db->join('tbluser', 'tbltest.author=tbluser.username');
		$this->db->join('tblcomment', 'tbltest.post_title=tblcomment.article_title');
		$this->db->where('tbltest.slug',  $slug );
		$this->db->where('tblcomment.approve_status', 'yes');
		$this->db->order_by("tblcomment.date_submitted", 'DESC');
        $query = $this->db->get();	

		if ($query->num_rows() > 0) 
        {
			return $query;
        } else {

			return false;
		}    
	} 

	function displayNoCommentPost($slug) {

		$this->db->select('tbltest.post_title, tbltest.body, tbltest.date_posted, tbltest.post_id, tbltest.post_image, tbltest.slug, tbltest.metatitle, tbltest.meta_description, tbltest.altext, tbltest.date_posted, tbluser.first_name, tbluser.last_name');
		$this->db->from('tbltest');
		$this->db->join('tbluser', 'tbltest.author=tbluser.username');
		$this->db->where('tbltest.slug',  $slug);
		$query = $this->db->get();
		return $query;
	}

	// dispaly deleted or published articles
	public function displayArticles($limit, $start,  $del_status) {    
        $this->db->limit($limit, $start);
        $this->db->select('tbltest.author, tbltest.post_title, tbltest.post_id, tbltest.slug, tbltest.date_posted, tbluser.first_name, tbluser.last_name');
        $this->db->from('tbltest');
        $this->db->join('tbluser', 'tbltest.author=tbluser.username');
		$this->db->where('tbltest.deleted',  $del_status);
		$this->db->order_by("date_posted", "desc"); 
        $query = $this->db->get();
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }    
            return $data;
        }
        return false;
    }
     
    function get_total_rec($del_status) {
		$this->db->select('*');
        $this->db->from('tbltest');
        $this->db->where('deleted', $del_status);
        $query = $this->db->get();

        return $query->num_rows();
	}
}
	
	