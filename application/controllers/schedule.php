<?php

class Schedule extends CI_Controller{

    public function view() {

        $data ['title'] = "Schedule";
        $data ['desc'] = "schedule description";
        $data ['username'] =  $this->session->userdata('username'); 
        $page = "schedule" ;

        $this->load->view('template/header', $data);
        $this->load->view('pages/'.$page);
        $this->load->view('template/footer');
    }
}