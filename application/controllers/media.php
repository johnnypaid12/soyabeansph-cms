<?php
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media extends CI_Controller {

    
    function index() {

        if($this->session->userdata('username') != '')  {   

            $data['title'] = 'Media directory';  
            $data['desc'] = 'description';  
            $data['user'] = $this->session->userdata('username');
            
            
            $this->load->helper('directory');
        
            $dir = "assets/img/"; // Your Path to folder
            $data['map'] = directory_map($dir);
            
            $this->load->view('template/headeradmin', $data);   
            $this->load->view('template/sidebar');    
            $this->load->view('pages/mediadisplay');     
        } else {
            
            redirect(base_url() . 'login');
        }     

    }

    function imageUploadPost()
	{
		$config['upload_path']   = './assets/img'; 
		$config['allowed_types'] = 'gif|jpg|png'; 
		$config['max_size']      = 1024;


      	$this->load->library('upload', $config);
		$this->upload->do_upload('file');
		print_r('Image Uploaded Successfully.');
        exit;
	}
}   