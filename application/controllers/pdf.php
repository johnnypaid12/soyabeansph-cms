<?php
 defined('BASEPATH') OR exit('No direct script access allowed'); 

 class pdf extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->driver('session');
        $this->load->library('pdf');
    }


 public function index() { 

         $data['title'] = 'Login area';  
           $data['desc'] = 'description';  
           $data ['user'] = $this->session->userdata('username');
           $this->load->view('template/headeradmin', $data);
           $this->load->view('template/sidebar');
           $this->load->view('pages/adminnew'); 
        
    // Get output html
    $html = $this->output->get_output();
    
    // Load pdf library
    $this->load->library('pdf');
    
    // Load HTML content
    $this->dompdf->loadHtml($html);
    
    // (Optional) Setup the paper size and orientation
    $this->dompdf->setPaper('A4', 'landscape');
    
    // Render the HTML as PDF
    $this->dompdf->render();
    
    // Output the generated PDF (1 = download and 0 = preview)
    $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
        
            
    }
}