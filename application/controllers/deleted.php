<?php  
 defined('BASEPATH') OR exit('No direct script access allowed');  

 class Deleted extends CI_Controller {  
     
    
    public function view() {

        if($this->session->userdata('username') != '')  {

            $data ['title'] = 'Post article';
            $data ['desc'] = ''; 
            $data['user'] = $this->session->userdata('username');
            $data ['length'] = 0;
            $page = "deleted" ;

            $this->load->model('blog_model');
            $delPost = $this->blog_model->displayDeleted();

            if (!$delPost) {
                
                echo "No records!";

            } else {
                $i=0;
                foreach ($delPost->result_array() as $row) {

                    $data ['author'][$i] = $row['author'];
                    $data ['post_title'][$i] = $row['post_title'];
                    $data ['date_posted'][$i] = $row['date_posted'];
                    $data ['first_name'][$i] = $row['first_name'];
                    $data ['last_name'][$i] = $row['last_name'];
                    $data ['post_id'] [$i] = $row['post_id'];
                    $data ['slug'] [$i] = $row['slug'];
                    $i++;
                }    
                    $data ['length'] = $delPost->num_rows();
                   // echo $delPost->num_rows();
                    $this->load->view('template/headeradmin', $data);
                    $this->load->view('template/sidebar');
                    $this->load->view('pages/'.$page);
            }
       

            


        } else  {  
            redirect(base_url() . 'login');  
        }  

        
        
    }
    function recoverBlog($post_id){

        $tag = "no";
        $this->load->model('blog_model');
        $test = $this->blog_model->RecDelPost($post_id, $tag);
        $this->session->set_flashdata('msg', 'Article recoverd!');  
        redirect(base_url() . 'deleted');
       
    }
    
    
}
