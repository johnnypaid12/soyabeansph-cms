<?php 

 defined('BASEPATH') OR exit('No direct script access allowed');  

 class Comment extends CI_Controller {  


        public function index()
        {
            if (!isset($_POST['submit'])) {
                redirect(base_url());
            } else {
                $this->load->helper(array('form', 'url'));
                $this->load->library('form_validation');

                $this->form_validation->set_rules('name', 'Name', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required');
                $this->form_validation->set_rules('text', 'Comment', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('msg', 'Please complete the form!');
                    redirect(base_url() . 'blog/links/' . $this->input->post('slug'));   
                } else {
                        $this->_addComment();
                }
            }
        }

    function _addComment() {
            $data= array(
                'article_title' => $this->input->post('post_title'),
                'slug' => $this->input->post('slug'),
                'full_name'=> $this->input->post('name'),
                'email'=> $this->input->post('email'),
                'message'=> $this->input->post('text'),
                'approve_status'=> 'no',
                'date_submitted'=> date('Y-m-d H:i:s'),
            );
            //echo $data['article_title'];
           $this->load->model('comment_model');
           $addComment = $this->comment_model->saveComment($data);
           
           if ($addComment == false) {
                $this->session->set_flashdata('msg', 'Sorry but we are under mentainance right now');
                redirect(base_url() . 'blog/links/' . $this->input->post('slug'));
           } else {
                $this->session->set_flashdata('msg', 'Thank you so much! Soyabeans will review your comment asap.');
                redirect(base_url() . 'blog/links/' . $this->input->post('slug'));
           }
    }

    function updateComment($commId) {
        $this->load->model('comment_model');
        $updateStatus = $this->comment_model->changeCommentStatus($commId);
        redirect(base_url() . 'admin/comments/');
    }

 }
     