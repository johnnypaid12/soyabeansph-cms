<?php

class Home extends CI_Controller{

    public function view() {

        $data ['title'] = "Soya Beans Japanese Language School Cebu";
        $data ['desc'] = "Soya Beans Japanese language school in Cebu offers affordable option to leaners. With our Japanese sensei we can help you prepare with your dream in traveling or working in Japan.!";
        $page = "home";

        $this->load->view('template/header', $data);
        $this->load->view('pages/'.$page);
        $this->load->view('template/footer');
    }
}