<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Paginationtest extends CI_Controller {

    public function __construct() {
        parent::__construct();
 
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->database();
        $settings = $this->config->item('pagination');
    }
     
    
    function deleted() {
        if($this->session->userdata('username') != '')  {
            $del_status = 'yes';    
            $pages = 'test';
            $base = 'deleted';
            $this->_showDeleted($del_status, $pages, $base);
        } else  {  
            redirect(base_url() . 'login');  
        }  
    }

    function published() {
        if($this->session->userdata('username') != '')  {
            $del_status = 'no';    
            $pages = 'test2';
            $base = 'published';
            $this->_showDeleted($del_status, $pages, $base);
        } else  {  
            redirect(base_url() . 'login');  
        }  
    }

    function _showDeleted($del_status, $pages, $base) {
        $data ['title'] = 'Admin Area';
        $data ['desc'] = ''; 
        $data['user'] = $this->session->userdata('username');

        $this->load->model('blog_model');
    
        // init params
        $params = array();
        $limit_per_page = 5;
        $page =  ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $total_records = $this->blog_model->get_total_rec($del_status);
    
    
        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $this->blog_model->displayArticles($limit_per_page, $page*$limit_per_page, $del_status);
                
            $config['base_url'] = base_url() . 'paginationtest/' . $base;
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

           // $this->config->load('pagination');
            
            $this->pagination->initialize($config);
                
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }
            $this->load->view('template/headeradmin', $data);
            $this->load->view('template/sidebar');
            $this->load->view('pages/'.$pages, $params);

    }

    // comments pagination
    function comments() {
        if($this->session->userdata('username') != '')  { 
        $data ['title'] = 'Comment Area';
        $data ['desc'] = ''; 
        $data['user'] = $this->session->userdata('username');
        $approve_status = 'no';

        $this->load->model('comment_model');
    
        // init params
        $params = array();
        $limit_per_page = 5;
        $base = 'comments';
        $pages = 'comment';
        $page =  ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $total_records = $this->comment_model->get_total_comment($approve_status);
    
    
        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $this->comment_model->getComment($limit_per_page, $page*$limit_per_page);
                
            $config['base_url'] = base_url() . 'paginationtest/' . $base;
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

           // $this->config->load('pagination');
            
            $this->pagination->initialize($config);
                
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }
            $this->load->view('template/headeradmin', $data);
            $this->load->view('template/sidebar');
            $this->load->view('pages/'.$pages, $params);
        } else  {  
            redirect(base_url() . 'login');  
        }  
    }
    
}