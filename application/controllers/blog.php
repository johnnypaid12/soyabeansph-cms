<?php 

class Blog extends CI_Controller {

    public function view() {
          
                $data ['title'] = 'Post article';
                $data ['desc'] = ''; 
                $data['user'] = $this->session->userdata('username');
                $data['blogsection'] = "Blog Section";
                $page = "blog" ;

                $this->load->view('template/headertest', $data);
                $this->load->view('pages/'.$page); 
                $this->load->view('template/footer', $data);
        }

        function links($slug) {
            
            $this->load->model('blog_model');
            $getblog = $this->blog_model->displayPost($slug);
            
            if ($getblog == false) {

                $getblog = $this->blog_model->displayNoCommentPost($slug);

                foreach($getblog->result_array() as $row) {

                    $data ['post_title'] = $row ['post_title'];
                    $data ['body'] = $row['body'];
                    $data ['date_posted'] = $row['date_posted'];
                    $data ['post_image'] = $row['post_image'];
                    $data ['metatitle'] = $row['metatitle'];
                    $data ['description'] = $row['meta_description'];
                    $data ['altext']  = $row['altext'];
                    $data ['first_name']  = $row['first_name'];
                    $data ['last_name']  = $row['last_name'];
                    $data ['userfile']  = $row['post_image'];
                    $data ['slug']  = $row['slug'];
                }

                $data ['length'] = 0;
                $data ['title'] = 'Post article';
                $data ['desc'] = ''; 
                $data['blogsection'] = $data ['post_title'];
                $page = "blogpage" ;
    
                $this->load->view('template/header', $data);
                $this->load->view('pages/'.$page ); 
                $this->load->view('template/footer', $data);
              

            } else {
                $i=0;
                foreach($getblog->result_array() as $row) {

                    $data ['post_title'] = $row ['post_title'];
                    $data ['body'] = $row['body'];
                    $data ['date_posted'] = $row['date_posted'];
                    $data ['post_image'] = $row['post_image'];
                    $data ['metatitle'] = $row['metatitle'];
                    $data ['description'] = $row['meta_description'];
                    $data ['altext']  = $row['altext'];
                    $data ['first_name']  = $row['first_name'];
                    $data ['last_name']  = $row['last_name'];
                    $data ['userfile']  = $row['post_image'];
                    $data ['slug']  = $row['slug'];
                    $data ['full_name'][$i]  = $row['full_name'];
                    $data ['date_submitted'][$i]  = $row['date_submitted'];
                    $data ['message'][$i]  = $row['message'];
                    $i++;
                }

                $data ['length'] = $i;
                $data ['title'] = 'Post article';
                $data ['desc'] = ''; 
                $data['blogsection'] = $data ['post_title'];
                $page = "blogpage" ;
    
                $this->load->view('template/header', $data);
                $this->load->view('pages/'.$page ); 
                $this->load->view('template/footer', $data);
            }

                   

        }
}
