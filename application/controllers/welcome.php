<?php
 
class Welcome extends CI_Controller
 
{


   public function __construct() {
 
       parent:: __construct();
 
       $this->load->helper("url");
       $this->load->database();
 
       $this->load->model("pagination_model");
 
       $this->load->library("pagination");
 
   }
 
   public function  index() {

      
       $config = array();
       $config["base_url"] = base_url() . "welcome/index";
       $config["total_rows"] = $this->pagination_model->record_count();
       $config["per_page"] = 5;
       $config["uri_segment"] = 3;
 
       $this->pagination->initialize($config);
 
       $test = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
       $data["results"] = $this->pagination_model->fetch_departments($config["per_page"], $test);
       $data["links"] = $this->pagination->create_links();
 
       $this->load->view("blog_listing", $data);
   }
}