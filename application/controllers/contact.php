<?php

class Contact extends CI_Controller{

    public function view() {

        $data ['title'] = "Contact page";
        $data ['desc'] = "Visit Soya Beans or give us a call! We will get back at you for your Japanese language education queries.";   
        $page = "contact" ;

        $this->load->view('template/header', $data);
        $this->load->view('pages/'.$page);
        $this->load->view('template/footer');
    }
}