<?php

class Post_Oldversion extends CI_Controller{

    public function view() {

        if($this->session->userdata('username') != '')  {

            $data ['title'] = "Post article";
            $data ['desc'] = ""; 
            $page = "post" ;

            $this->load->view('template/headeradmin', $data);
            $this->load->view('pages/'.$page);


        } else  {  
            redirect(base_url() . 'login');  
        }  
    }

    public function addpost() {

        //search image
        $imgPost = $this->getimage($_FILES['userfile']['name']);

        if ($imgPost == "false") {
         
            //upload img

            $config['upload_path'] = './assets/img';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '10048';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';
            
            $this->load->library('upload', $config);

            

            if ( !$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());
                $imgPost = "default.jpg";
            } else {
                $data = array('upload_data' => $this->upload->data());
                $imgPost = $_FILES['userfile']['name'];
            }

            // end upload

            $data = array (
                'title' => $this->input->post('title'),
                'body' => $this->input->post('editor1'),
                'status' => $this->input->post('category_id'),
                'date' => date('Y-m-d H:i:s'),
                'image' => $imgPost
            );

            
            $this->load->model('blog_model');
            $this->blog_model->saveData($data);
            $this->session->set_flashdata('msg', 'Successfully added!');
            redirect(base_url() . 'post'); 

         } else {
            $this->session->set_flashdata('msg', 'Image already exist!');
            redirect(base_url() . 'post'); 
            //echo $upload_data['file_name'];
         }
        
    }

        function getimage ($imgPost) {

            $path = './assets/img/' . $imgPost;
            
            
            if (file_exists($path)) {
                return "true";
            } else {
                return "false";
            }


        }

}