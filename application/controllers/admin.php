<?php  
 defined('BASEPATH') OR exit('No direct script access allowed');  

 class Admin extends CI_Controller {  
     
    
      function login()  
      {  
           // main/login  
           $data['title'] = 'Login area';  
           $data['desc'] = 'description';  
           $this->load->view('template/header', $data);
           $this->load->view("pages/login");  
           $this->load->view('template/loginfooter');
      }  
 
      function login_validation()  
      {  
           $this->load->library('form_validation');  
           $this->form_validation->set_rules('username', 'Username', 'required');  
           $this->form_validation->set_rules('password', 'Password', 'required');  
           if ($this->form_validation->run())  
           {  
                $username = $this->input->post('username');  
                $password = $this->input->post('password');  
               
                //model function  
                $this->load->model('login');  
                if($this->login->getUsers($username, $password))  
                {  
                     $session_data = array(  
                          'username'     =>     $username 
                        );  
                     $this->session->set_userdata($session_data);  
                     //redirect(base_url() . 'admin'); 
                     $this->_enter();
                }  
                else  
                {  
                     $this->session->set_flashdata('error', 'Invalid Username and Password');  
                     redirect(base_url() . 'login');  
                }  
           }  
           else  
           {  
                //false  
                $this->login();  
           }  
      }

      function _enter(){  
            if($this->session->userdata('username') != '')  {   
               
                // $data ['title'] = "admin area kini";
                // $data ['desc'] = "admin";
                // $data ['user'] = $this->session->userdata('username');
                // $page = "adminnew";

                // // retrieve articles for dashboard
                // $this->load->model('blog_model');
                // $test = $this->blog_model->getPost();

                // if ($test->num_rows() < 1 ) {
                //     echo "No records to show!";
                //     $data ['length'] = 0;
                    
                //     $this->load->view('template/headeradmin', $data);
                //     $this->load->view('template/sidebar');
                //     $this->load->view('pages/'.$page); 
                // } else {
                //     //add querry results to data array...
                //     $i=0;
                //     foreach ($test->result_array() as $row)
                //         {
                //                 $data ['author'][$i] = $row['author'];
                //                 $data ['post_title'][$i] = $row['post_title'];
                //                 $data ['date_posted'][$i] = $row['date_posted'];
                //                 $data ['first_name'][$i] = $row['first_name'];
                //                 $data ['last_name'][$i] = $row['last_name'];
                //                 $data ['post_id'] [$i] = $row['post_id'];
                //                 $data ['slug'] [$i] = $row['slug'];
                //                 $i++;
                //         }    
                //                 $data ['length'] = $test->num_rows();
                //         //echo $test->num_rows();
                        
                //         $this->load->view('template/headeradmin', $data);
                //         $this->load->view('template/sidebar');
                //         $this->load->view('pages/'.$page); 
                // }
                redirect(base_url() . 'paginationtest/published');

            }  else  {  
                redirect(base_url() . 'login');     
            }  
        }

 
        public function logout()   	{  

                $this->session->unset_userdata('username');  
                redirect(base_url() . 'login');  
        }  
                
 }  