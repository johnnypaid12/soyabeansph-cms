<?php

class Post extends CI_Controller{

    

    public function view() {

        if($this->session->userdata('username') != '')  {

            $data ['title'] = 'Post article';
            $data ['desc'] = ''; 
            $data['user'] = $this->session->userdata('username');
            $page = "post" ;

            $this->load->view('template/headeradmin', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('pages/'.$page);


        } else  {  
            redirect(base_url() . 'login');  
        }  
    }

        function initializePost() {

        $data = array (
            'post_title' => $this->input->post(trim('title')),
            'body' => $this->input->post('editor1'),
            'post_status' => $this->input->post('category_id'),
            'date_posted' => date('Y-m-d H:i:s'),
            'metatitle' => $this->input->post('metatitle'),
            'meta_description' => $this->input->post('description'),
            'altext' => $this->input->post('altext'),
            'slug' => url_title($this->input->post(trim('title')), 'dash', true),
            'author' => $this->session->userdata('username'),
            'deleted' => "no"
        );

        if (!(trim($data['post_title'])) == "" ) {
  
           // $imgSearch = $this->getImage($_FILES['userfile']['name']); //search image if it exist!

          // if ($imgSearch == false) {

                $imgPost = $this->uploadImage();

            //     $flashMessage = 'Successfully added!';
                $data['post_image'] = $imgPost;
                $this->createPost($data);
                

            // } else {
            //     $flashMessage = 'Article added but image already exist, default image is being used instead!';
            //     $data['post_image'] = "default.jpg";
            //     $this->createPost($data, $flashMessage); 
        
            // }

        } else {
            $this->session->set_flashdata('msg', 'Please fillup the title!');
            redirect(base_url() . 'post');

        }
        
    }

    function createPost($data) {

        $this->load->model('blog_model');     
        $chkTitle = $this->blog_model->saveData($data);
        if ($chkTitle == false) {
            
            $this->session->set_flashdata('msg', 'Article not submitted.. Title already exist!');
            redirect(base_url() . 'post');
            
        } else {

            $this->session->set_flashdata('msg', 'Added!');
            redirect(base_url() . 'post');
        }

    }


    // function getImage($imgSearch) {

    //     $path = './assets/img/' . $imgSearch;
            
    //     if (!file_exists($path) || ($imgSearch == "")) {
    //         return false;
    //     } else {
    //         return true;
    //     }
	// }


	function uploadImage() {

		$config['upload_path'] = './assets/img';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '10048';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
            
        $this->load->library('upload', $config);
        $this->load->helper('file');

        if ( !$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $imgPost = "default.jpg";
        } else {
            $data = array('upload_data' => $this->upload->data());
            $imgPost = $this->upload->data('raw_name');
		}
		return $imgPost . $this->upload->data('file_ext') ;
    }


    function deleteBlog($post_id = 0){

        $tag = "yes";
        $this->load->model('blog_model');
        $test = $this->blog_model->searchPost($post_id);

        if($test->result() == false) {
            $this->session->set_flashdata('msg', 'No article to delete!');
            redirect(base_url() . 'paginationtest/published');
        } else {
            $test = $this->blog_model->RecDelPost($post_id, $tag);
            $this->session->set_flashdata('msg', 'Article deleted!'); 
            redirect(base_url() . 'paginationtest/published');  
        }

    }


    function editBlog($post_id = 0) {
        //echo "please edit article " . $post_id;
        $this->load->model('blog_model');
        $edit = $this->blog_model->searchPost($post_id);

        if($edit->result() == false) {
        
            $this->session->set_flashdata('msg', 'No article to update!');
            redirect(base_url() . 'paginationtest/published');
        } else {
            //echo $post_id;
          
            foreach ($edit->result_array() as $row) {

                $data ['post_title'] = $row['post_title'];
                $data ['body'] = $row['body'];
                $data ['metatitle'] = $row['metatitle'];
                $data ['description'] = $row['meta_description'];
                $data ['category_id'] = $row['post_status'];
                $data ['altext']  = $row['altext'];
                $data ['userfile']  = $row['post_image'];
            }    

            //echo $data ['post_title'];   
                $data ['title'] = 'Post article';
                $data ['desc'] = ''; 
                $data['user'] = $this->session->userdata('username');
                $page = "updatepost" ;

                $this->load->view('template/headeradmin', $data);
                $this->load->view('template/sidebar', $data);
                $this->load->view('pages/'.$page); 
        }
    }

    function updateBlog() {

        if($this->session->userdata('username') != '')  {
        $data = array (
            'post_title' => $this->input->post(trim('title')),
            'body' => $this->input->post('editor1'),
            'post_status' => $this->input->post('category_id'),
            'date_updated' => date('Y-m-d H:i:s'),
            'metatitle' => $this->input->post('metatitle'),
            'meta_description' => $this->input->post('description'),
            'altext' => $this->input->post('altext'),
            'slug' => url_title($this->input->post(trim('title')), 'dash', true),
            'author' => $this->session->userdata('username'),
            'deleted' => "no"
        );

            echo $data['post_image'];
            $this->load->model('blog_model');
            $this->blog_model->updatePost($data);

            $test = $this->blog_model->RecDelPost($post_id, $tag);
            $this->session->set_flashdata('msg', '"' . $data['post_title'] . '"' . ' has been updated!'); 
            redirect(base_url() . 'paginationtest/published');

        } else {

            $this->session->set_flashdata('msg', 'You are not login!'); 
            redirect(base_url() . 'post');
        }
    }
    
}