<?php

class User extends CI_Controller {

    public function view() {

        if($this->session->userdata('username') != '')  {

            $data ['title'] = 'Add User';
            $data ['desc'] = ''; 
            $data['user'] = $this->session->userdata('username');
            $page = "user" ;

            $this->load->view('template/headeradmin', $data);
            $this->load->view('template/sidebar', $data);
            $this->load->view('pages/'.$page);


        } else  {  
            redirect(base_url() . 'login');  
        }  
    }


    function register() {
    

        if($this->session->userdata('username') != '')  {


			$this->form_validation->set_rules('fname','First Name','trim|required');
			$this->form_validation->set_rules('lname','Last Name','trim|required');
			$this->form_validation->set_rules('username','Username','required|callback_username_check');
			$this->form_validation->set_rules('password','Password','trim|required');
            $this->form_validation->set_rules('passconf','Confirm Password','trim|matches[password]');
            
			if($this->form_validation->run() === FALSE){
                
                $this->view();

			} else {
				// Encrypt password
                $enc_password = md5($this->input->post('password'));

                $data = array(
                    'first_name' => $this->input->post('fname'),
                    'last_name' => $this->input->post('lname'),
                    'username' => $this->input->post('username'),
                    'user_password' => $enc_password,
                    'date_registered' => date('Y-m-d H:i:s')
                ); 
              
                // echo $data['password'];
                $this->load->model('user_model');
                $test = $this->user_model->registerUser($data);
				// Set message 
				$this->session->set_flashdata('msg','User is now registered and can log in!');
                //redirect('user');
                redirect('user');
               
	
			}

        } else  {  

            redirect(base_url() . 'login');  
        
        }  
    }

    public function username_check($str = '') {
        if($this->session->userdata('username') != '')  {
            $this->form_validation->set_message('username_check','That username is taken. Please choose a different one');

            $this->load->model('user_model');
            if(!($this->user_model->checkUserName($str))) {
                return false;
            } else {
                return true;
            }
            // echo $str;
        }    else {
            redirect(base_url() . 'login'); 
        }


    }

}