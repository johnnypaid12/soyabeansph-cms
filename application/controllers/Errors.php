<?php

class Errors extends CI_Controller{

    public function view() {

        $data ['title'] = "Page not found!";
        $data ['desc'] = "Sorry page not found.";
        $page = "404" ;

        $this->load->view('template/header', $data);
        $this->load->view('pages/'.$page);
        $this->load->view('template/footer');
    }
}